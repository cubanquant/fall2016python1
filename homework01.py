from __future__ import division, print_function, absolute_import

FACT_NUMBER = 5
RATE = 5/100.

print("Rate = {}".format(RATE))

def fact(x):
    """
    Returns the factorial of x
    """
    if x == 0: return 1

    product = 1
    for i in xrange(1, x+1):
        product = i * product

    return product


def main():
    """Main entry point function"""
    print(fact(FACT_NUMBER))

print(__name__)

if __name__ == "__main__":
    print("Calling main() from homework01.py")
    main()
